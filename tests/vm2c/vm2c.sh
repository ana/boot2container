source ./tests/vm2c/base.sh

testBootingWithDefault() {
    DISK_PATH=cache.qcow2
    reset_disk
    run_test

    assertEquals "0" "$exit_code"
    assertContains "$stdout" "Mounting the partition /dev/vda1 to /storage: DONE"
    assertContains "$stdout" "Getting the time from the NTP server pool.ntp.org: DONE"
    assertContains "$stdout" "hello world!"
    assertContains "$stdout" "Execution is over, pipeline status: 0"
    assertNotContains "$stdout" "I couldn't get the result of the pipeline, marking the job as failed"
}
suite_addTest testBootingWithDefault

testUpdateData() {
    DISK_PATH=cache.qcow2
    reset_disk

    touch file_to_find.txt
    rm -f new_file.txt

    export B2C_COMMAND="ls file_to_find.txt; touch new_file.txt"

    run_test

    assertEquals "0" "$exit_code"
    assertTrue "The script has been run" "[ -f new_file.txt ]"

    rm new_file.txt
    rm file_to_find.txt
}
suite_addTest testUpdateData

testFail() {
    DISK_PATH=cache.qcow2
    reset_disk

    export B2C_COMMAND="ls this_file_does_not_exist.txt"

    run_test

    assertEquals "1" "$exit_code"
}
suite_addTest testFail
