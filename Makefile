# Copyright (c) 2021-2022 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Martin Roukala <martin.roukala@mupuf.org>
#

.PHONY: default
default: build ;

SHELL := /bin/bash
.ONESHELL:

YQ ?= $(shell which yq 2> /dev/null)
ifeq (, $(YQ))
  $(error "The yq binary can't be found in $$PATH. Install it using pip3 install yq.")
endif

# valid values are the same as what GOARCH expects,
# see: go tool dist list | cut -d'/' -f2 | sort -u
ARCH ?= amd64
LINUX_OUTPUT_NAME = linux-$(LINUX_ARCH)
ifeq ($(ARCH), amd64)
	QEMU = qemu-system-x86_64
	QEMU_MANUAL_TEST_EXTRA =
	UNAME_ARCH = x86_64
	LINUX_ARCH = x86_64
	CROSS_COMPILE = x86_64-pc-linux-gnu-
	LINUX_BIN_PATH ?= arch/x86_64/boot/bzImage
	EFI_BIOS_URL ?=
else ifeq ($(ARCH), arm64)
	QEMU = qemu-system-aarch64 -machine virt -cpu max
	QEMU_MANUAL_TEST_EXTRA = -drive if=pflash,format=raw,file=out/$(ARCH)-efi.img,readonly=on
	UNAME_ARCH = aarch64
	LINUX_ARCH = arm64
	CROSS_COMPILE ?= aarch64-linux-gnu-
	LINUX_BIN_PATH ?= arch/arm64/boot/vmlinuz.efi
	LINUX_OUTPUT_NAME = linux-$(LINUX_ARCH).efi
	EFI_BIOS_URL ?= https://gitlab.freedesktop.org/mupuf/boot2container/-/package_files/147/download
else ifeq ($(ARCH), arm)
	# TODO: Add a working UEFI bootloader
	QEMU = qemu-system-arm -machine virt
	QEMU_MANUAL_TEST_EXTRA =
	UNAME_ARCH = armv7l
	LINUX_ARCH = arm
	CROSS_COMPILE ?= arm-none-eabi-
	LINUX_BIN_PATH ?= arch/arm/boot/zImage
	EFI_BIOS_URL ?=
else
$(error Unknown architecture. Supported architectures: amd64, arm64, arm)
endif
ifeq ($(shell uname -m), $(UNAME_ARCH))
	LINUX_MAKE_CMDLINE = ARCH=$(LINUX_ARCH)
	QEMU := $(QEMU) -enable-kvm
else
	LINUX_MAKE_CMDLINE = ARCH=$(LINUX_ARCH) CROSS_COMPILE=$(CROSS_COMPILE)
endif
LINUX_BIN = $(PWD)/out/$(LINUX_OUTPUT_NAME)
LINUX_FIRMWARE = $(PWD)/deps/linux-firmware

DOCKER ?= $(shell which docker 2>/dev/null || which podman 2>/dev/null)
ALPINE_VERSION ?= $(shell $(YQ) -r '.variables.ALPINE_VERSION' .gitlab-ci.yml)
IMAGE_BASE_TAG ?= $(shell $(YQ) -r '.variables.BASE_TAG' .gitlab-ci.yml)
IMAGE_LABEL ?= registry.freedesktop.org/mupuf/boot2container/alpine/$(ALPINE_VERSION):$(IMAGE_BASE_TAG)

CONTAINER_LABEL ?= boot2container-$(ARCH)
B2C_KERNEL_RELEASE = v0.9.8

INTEGRATION ?= 1
UNITTEST ?= 1
VM2C ?= 1

# TODO: Collect all wanted modules and load them
out/initramfs.linux_$(ARCH).cpio:
	@mkdir -p out usr_mods
	@rm out/initramfs.linux_$(ARCH).cpio 2> /dev/null || /bin/true
	@-$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null 2> /dev/null || /bin/true
	$(DOCKER) create --platform linux/$(ARCH) --env GOARCH=$(ARCH) --name $(CONTAINER_LABEL) -v $(PWD):/app --entrypoint /app/container/entrypoint.sh $(IMAGE_LABEL)
	$(DOCKER) start -a $(CONTAINER_LABEL)
	@$(DOCKER) cp $(CONTAINER_LABEL):/tmp/initramfs.linux_$(ARCH).cpio out/ > /dev/null
	@$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null

out/initramfs.linux_$(ARCH).cpio.xz: out/initramfs.linux_$(ARCH).cpio
	xz --check=crc32 -9 --lzma2=dict=1MiB --stdout out/initramfs.linux_$(ARCH).cpio | dd conv=sync bs=512 of=out/initramfs.linux_$(ARCH).cpio.xz

rebuild_container:
	$(DOCKER) build --platform linux/$(ARCH) --build-arg ALPINE_VERSION=$(ALPINE_VERSION) -t $(IMAGE_LABEL) .
rebuild-container: rebuild_container

out/disk.img:
	mkdir -p $$(dirname "$@")
	fallocate -l 128M out/disk.img

build-fast: out/initramfs.linux_$(ARCH).cpio
build: out/initramfs.linux_$(ARCH).cpio.xz

$(LINUX_FIRMWARE):
	git clone --depth=1 https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git $(LINUX_FIRMWARE)

$(LINUX_BIN): FEATURES ?= "common,netfilter,network,qemu,sensors,serial_adapters"
$(LINUX_BIN): CONFIRM=1
$(LINUX_BIN): $(LINUX_FIRMWARE)
	@[ -d "$(LINUX_SRC)" ] || {
		echo "ERROR: LINUX_SRC is a required parameter, and should be the path to the Linux kernel source code"
		exit 1
	}
	echo "# Compiling the Linux kernel with the following features (FEATURES=...): $(FEATURES)"
	pushd "${PWD}/config/linux/" > /dev/null
	esh="${PWD}/deps/esh - arch=$(ARCH) features=$(FEATURES)"
	cat `echo "$(FEATURES)" | tr "," " "` | $$esh > /tmp/linux_additional_config
	popd > /dev/null
	pushd "$(LINUX_SRC)" > /dev/null
	make="$(MAKE) $(LINUX_MAKE_CMDLINE)"
	echo -e "\n# Generating the defconfig configuration"
	$$make defconfig > /dev/null
	echo -e "\n# Appending our features to the defconfig"
	cp .config /tmp/linux_def_config
	cat /tmp/linux_def_config /tmp/linux_additional_config > .config  # TODO: Check that we never specify conflicting parameters
	sed -iE 's/=m$$/=y/' .config  # Compile everything built-in, no modules!
	echo "CONFIG_EXTRA_FIRMWARE_DIR=\"$(LINUX_FIRMWARE)\"" >> .config
	FIRMWARES=
	for pattern in $$(sed -n 's|^# B2C_FW+=||p' .config | tr '\n' ' '); do
		find $(LINUX_FIRMWARE) -path "$(LINUX_FIRMWARE)" -type f -printf '%P\n' | tr '\n' ' '
		FIRMWARES="$${FIRMWARES}$$(set -o noglob; find $(LINUX_FIRMWARE) -path $(LINUX_FIRMWARE)/$$pattern -type f -printf '%P\n' | tr '\n' ' ')"
	done
	echo "CONFIG_EXTRA_FIRMWARE=\"$$FIRMWARES\"" >> .config
	echo -e "\n# Running olddefconfig on the resulting configuration"
	$$make olddefconfig |& grep -v "warning: override: " || exit 1
	# Check that our changes have not been overwritten
	echo -e "\n# Checking for config options that got overridden"
	has_warnings=0
	for feature in `echo "$(FEATURES)" | tr "," " "`; do
		for config in `cat $(PWD)/config/linux/$$feature | $$esh | sed -n '/^CONFIG_.*=y$$/p'`; do
			if ! grep -q "$$config" .config; then
				echo "WARNING: $$feature's config option \`$$config\` is missing from the final configuration"
				has_warnings=1
			fi
		done
	done
	if [[ "$$has_warnings" -eq 1 && "$(CONFIRM)" -eq 1 ]]; then
		# If we are in a user terminal, ask the user to confirm they want to continue. Otherwise, abort.
		if [ -t 0 ]; then
			echo -e "\nSome options were found missing. Please review then press any key to continue."
			read
		else
			echo -e "\nSome options were found missing. Aborting!"
			exit 1
		fi
	fi
	echo -e "\n# Compiling the kernel"
	$$make || exit 1
	echo -e "\n# Copying the kernel to $(LINUX_BIN)"
	mkdir -p $$(dirname $(LINUX_BIN))
	cp $(LINUX_BIN_PATH) $(LINUX_BIN)
	popd > /dev/null
	echo -e "\n# Done \o/"

linux: $(LINUX_BIN)

# EFI bootloader
out/$(ARCH)-efi.img:
	mkdir -p $$(dirname "$@")
	if [ -n "${EFI_BIOS_URL}" ]; then curl -o out/$(ARCH)-efi.img "${EFI_BIOS_URL}" || (echo "ERROR: Could not download the EFI firmware"; exit 1); truncate -s 64m out/$(ARCH)-efi.img; fi

test: KERNEL ?= $(LINUX_BIN)
test: build-fast $(KERNEL) out/disk.img out/$(ARCH)-efi.img
	[ -f "$(KERNEL)" ] || wget -O "$(KERNEL)" https://gitlab.freedesktop.org/mupuf/boot2container/-/releases/$(B2C_KERNEL_RELEASE)/downloads/linux-$(LINUX_ARCH)

	[[ "$(ARCH)" == "amd64" ]] || {
		echo "ERROR: Tests are only supported on the amd64 architecture. You may however run your arm64 initrd using 'make manual test'."
		exit 1
	}
	$(DOCKER) run --rm --name b2c_test --device=/dev/kvm -v $(PWD)/out/:/out --env B2C_INITRD=/out/initramfs.linux_$(ARCH).cpio -v $(KERNEL):/tmp/kernel --env B2C_KERNEL=/tmp/kernel --env EFI_PATH=/out/$(ARCH)-efi.img -v $(PWD):/app -v $(PWD)/config/keymaps/:/usr/share/keymaps/ --env "QEMU=$(QEMU)" --entrypoint /app/tests/tests.sh --env UNITTEST=$(UNITTEST) --env INTEGRATION=$(INTEGRATION) --env VM2C=$(VM2C) $(IMAGE_LABEL)

manual_test: KERNEL ?= $(LINUX_BIN)
manual_test: build-fast out/disk.img out/$(ARCH)-efi.img
	[ -f "$(KERNEL)" ] || wget -O "$(KERNEL)" https://gitlab.freedesktop.org/mupuf/boot2container/-/releases/$(B2C_KERNEL_RELEASE)/downloads/linux-$(LINUX_ARCH)
	$(QEMU) $(QEMU_MANUAL_TEST_EXTRA) -drive file=out/disk.img,format=raw,if=virtio -netdev user,id=hostnet0 -device virtio-net-pci,netdev=hostnet0 -kernel $(KERNEL) -initrd out/initramfs.linux_$(ARCH).cpio -nographic -m 512M -smp 4 -append 'console=ttyS0 b2c.run=docker://docker.io/library/hello-world b2c.run="-ti docker://docker.io/library/alpine:latest" b2c.cache_device=auto'
manual-test: manual_test

clean:
	-rm -rf out/
	$(DOCKER) ps -a --format '{{.Names}}' | grep $(CONTAINER_LABEL) 2>/dev/null && $(DOCKER) rm $(CONTAINER_LABEL)
