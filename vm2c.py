#!/usr/bin/env python3

# Copyright 2022 - Benjamin Tissoires
# SPDX-License-Identifier: MIT

import inspect
import json
import os
import platform
import click
import shlex
import subprocess
import sys
import time
import urllib.parse
import urllib.request

from pathlib import Path
from rich.console import Console
from typing import Tuple, List

console = Console(force_terminal=True)


def section_start(section_name: str, title: str, collapsed: bool = True) -> None:
    # using rich here mangles the '\r' chars and then the sections, use plain ansi codes
    collapsed_cmd = "[collapsed=true]" if collapsed else ""
    with console.capture() as capture:
        console_print(
            f"[bold blue]{title}[/bold blue]",
            highlight=False,
        )
    str_output = capture.get()
    print(
        f"\r\033[0Ksection_start:{int(time.time())}:{section_name}{collapsed_cmd}\r\033[0K{str_output}"
    )


def section_end(section_name: str) -> None:
    # using rich here mangles the '\r' chars and then the sections, use plain ansi codes
    print(f"\r\033[0Ksection_end:{int(time.time())}:{section_name}\r\033[0K")


def get_kernel_arch() -> str:
    arch = platform.machine().lower()
    if arch == "aarch64":
        return "arm64"
    return arch


def get_initrd_arch() -> str:
    arch = get_kernel_arch()
    if arch == "x86_64":
        return "amd64"
    return arch


def get_latest_b2c_release() -> str:
    # mupuf/boot2container is 8765
    project = 8765
    with urllib.request.urlopen(
        f"https://gitlab.freedesktop.org/api/v4/projects/8765/releases"
    ) as f:
        data = json.load(f)
        return data[0]["tag_name"]
    # something went off, return a known tag
    return "v0.9.6"


def console_print(data, **kwargs):
    console.print(
        data,
        crop=False,
        overflow="ignore",
        **kwargs,
    )


@click.option(
    "--version",
    default=None,
    help="the boot2container kernel and initramfs version to use (defaults to the latest release)",
)
@click.option(
    "--kernel",
    default=None,
    help="the kernel that qemu will be running (needs to conform to https://gitlab.freedesktop.org/mupuf/boot2container/-/blob/master/README.md)",
)
@click.option(
    "--initrd",
    default=None,
    help="the b2c initrd image",
)
@click.option(
    "--image",
    default="harbor.freedesktop.org/freedesktop/ci-templates/ci-fairy:ci-fairy-base-image-2022-10-20",
    help="the container image to run in b2c (it needs to have at least the sh executable)",
)
@click.option(
    "--helper_image",
    default="harbor.freedesktop.org/freedesktop/ci-templates/ci-fairy:ci-fairy-base-image-2022-10-20",
    help="the container image to run in b2c for pre/post containers",
)
@click.option(
    "--command",
    default="echo hello world!",
    help="the command to start in b2c. Note that using quotes (single or double) may/will result in a wrong behavior",
)
@click.option(
    "--workdir",
    default="/app",
    type=click.Path(exists=True),
    help="the working directory of vm2c to store the kernel, initramfs, etc...",
)
@click.option(
    "--memory",
    default="4G",
    help="how much memory should be allocated to qemu (default is 4G)",
)
@click.option(
    "--qemu_args",
    default="",
    help='any extra args forwarded to qemu for example: "-usb -device usb-host,vendorid=0x046d,productid=0xc52b"',
)
@click.option(
    "--qemu_bin",
    default=f"qemu-system-{platform.machine().lower()}",
    help="The qemu binary to use, defaults to the current architecture qemu",
)
@click.option(
    "--dry-run",
    is_flag=True,
    help="Do not run the qemu command, just print it",
)
@click.command()
def vm2c(
    version: str,
    kernel: str,
    initrd: str,
    image: str,
    helper_image: str,
    command: str,
    workdir: Path,
    memory: str,
    qemu_args: str,
    qemu_bin: str,
    dry_run: bool,
) -> None:

    # Initial warning about the unstable interface
    console_print(f"[bold yellow]WARNING: This script is experimental. Its interface is subject to changes.\n")

    workdir = Path(workdir)

    # look for the kernel parameter:
    # if None is provided, download only if there is no bzImage file present
    if kernel is None:
        kernel = workdir / "bzImage"

        if not kernel.is_file():
            if version is None:
                version = get_latest_b2c_release()
            kernel = f"https://gitlab.freedesktop.org/mupuf/boot2container/-/releases/{version}/downloads/linux-{get_kernel_arch()}"

    # look for the initramfs parameter:
    # if None is provided, download only if there is no initramfs.cpio.xz file present
    if initrd is None:
        initrd = workdir / "initramfs.cpio.xz"

        if not initrd.is_file():
            if version is None:
                version = get_latest_b2c_release()
            initrd = f"https://gitlab.freedesktop.org/mupuf/boot2container/-/releases/{version}/downloads/initramfs.linux_{get_initrd_arch()}.cpio.xz"

    # force the use of the registry mirror, to reduce bandwidth
    image = image.replace("registry.freedesktop.org", "harbor.freedesktop.org/cache")

    # encapsulate the command in sh:
    if not command.startswith("sh "):
        command = f"sh -c '{command}'"

    # force escape quotes
    command = command.replace('"', '\\"')

    console_print(
        f"[bold green] Starting [bold red]vm2c[bold green] with the following parameters:"
    )
    for key in click.get_current_context().params:
        value = locals()[key]
        if not value:
            value = "''"
        console_print(f"    [bold blue]B2C_{key.upper()}[/bold blue]: {value}")

    #####################################################################################
    section_start("download", "Downloading components")
    # ...................................................................................

    # kernel
    parsed = urllib.parse.urlparse(str(kernel))
    if parsed.scheme:
        # download required
        kernel_path = str(workdir / "bzImage")
        result = subprocess.run(["curl", "-L", "-o", kernel_path, kernel])
        if result.returncode != 0:
            sys.exit(result.returncode)

        kernel = kernel_path
    else:
        console_print(f"preserving existing {str(kernel)}")

    # initrd
    parsed = urllib.parse.urlparse(str(initrd))
    if parsed.scheme:
        # download required
        initrd_path = str(workdir / "initramfs.cpio.xz")
        result = subprocess.run(["curl", "-L", "-o", initrd_path, initrd])
        if result.returncode != 0:
            sys.exit(result.returncode)

        initrd = initrd_path
    else:
        console_print(f"preserving existing {str(initrd)}")

    # ...................................................................................
    section_end("download")
    #####################################################################################

    #####################################################################################
    section_start("prep", "Preparing local data")
    # ...................................................................................

    cache = workdir / "cache.qcow2"
    if cache.is_file():
        console_print(f"preserving existing cache at {cache}")
    else:
        console_print(f"initialising cache at {cache}")
        result = subprocess.run(["qemu-img", "create", "-f", "qcow2", str(cache), "4G"])
        if result.returncode != 0:
            sys.exit(result.returncode)

    post_script = workdir / "b2c_post.sh"
    with open(post_script, "w") as f:
        f.write(
            f"""#!/bin/sh

env > {str(workdir)}/.b2c_env
echo B2C_POST_EXECUTED=1 >> {str(workdir)}/.b2c_env"""
        )

    cwd = Path.cwd()

    b2c_cwdfs, b2c_cwd_volume = (
        (
            'b2c.filesystem="cwdfs,src=cwdfs,type=9p" b2c.volume="cwdfs,filesystem=cwdfs"',
            f"-v cwdfs:{str(cwd)}",
        )
        if workdir != cwd
        else ("", "")
    )

    # store the current environment in a file so we can reuse it in podman
    # we need to escape the newlines in environment variables because
    # gitlab likes them
    b2c_env = workdir / ".b2c_env"
    console_print(f"[green]storing current environment in {str(b2c_env)}")
    with open(b2c_env, "w") as f:
        for key, value in os.environ.items():
            value = value.replace("\n", "\\n")
            f.write(f"{key}={value}\n")

    # remove leftovers from previous run
    post = workdir / ".b2c_post"
    post.unlink(missing_ok=True)

    # ...................................................................................
    section_end("prep")
    #####################################################################################

    #####################################################################################
    section_start("b2c_kernel_boot", f"starting [red]{command}[/red] in qemu")
    # ...................................................................................

    qemu_command = [qemu_bin]
    qemu_command.extend(["-drive", f"file={str(cache)},format=qcow2,if=virtio"])
    qemu_command.extend(["-netdev", "user,id=hostnet0"])
    qemu_command.extend(["-device", "virtio-net-pci,netdev=hostnet0"])
    qemu_command.extend([f"-kernel", f"{str(kernel)}"])
    qemu_command.extend([f"-initrd", f"{str(initrd)}"])
    qemu_command.extend([f"-display", "none"])
    qemu_command.extend([f"-serial", "mon:stdio"])
    qemu_command.extend([f"-m", f"{memory}"])
    qemu_command.extend([f"-enable-kvm"])
    qemu_command.extend([f"-cpu", "host"])
    qemu_command.extend(
        [
            f"-virtfs",
            f"local,path={str(workdir)},mount_tag=qemufs,security_model=passthrough",
        ]
    )
    qemu_command.extend(
        [
            f"-virtfs",
            f"local,path={str(cwd)},mount_tag=cwdfs,security_model=passthrough",
        ]
    )

    # WARNING: Do not use new b2c features in here until they got into a release!
    # Users of this script will always fetch it from master.
    kernel_cmd_line = "console=ttyS0 "
    kernel_cmd_line += "b2c.ntp_peer=auto "
    kernel_cmd_line += 'b2c.filesystem="qemufs,src=qemufs,type=9p" '
    kernel_cmd_line += 'b2c.volume="qemufs,filesystem=qemufs" '
    if b2c_cwdfs:
        kernel_cmd_line += f"{b2c_cwdfs} "
    kernel_cmd_line += f'b2c.container="--workdir {str(cwd)} -v qemufs:{str(workdir)} {b2c_cwd_volume} --env-file=qemufs/.b2c_env -ti -v /dev:/dev {image} {command}" '
    kernel_cmd_line += f'b2c.post_container="-ti -v qemufs:{str(workdir)} --env-file=qemufs/.b2c_env {helper_image} sh {str(workdir)}/b2c_post.sh" '
    kernel_cmd_line += "b2c.cache_device=auto "
    # WARNING: Do not use new b2c features in here until they got into a release!
    # Users of this script will always fetch it from master.

    qemu_command.extend(["-append", kernel_cmd_line])
    qemu_command.extend(shlex.split(qemu_args))

    console_print(qemu_command)

    if not dry_run:
        result = subprocess.run(qemu_command)
        if result.returncode != 0:
            sys.exit(result.returncode)

    # ...................................................................................
    section_end("b2c_kernel_boot")
    #####################################################################################

    #####################################################################################
    section_start("gather_results", "Gather results", collapsed=False)
    # ...................................................................................

    result_code = 1
    executed = False
    console_print(f"parsing environment at {str(b2c_env)}")
    with open(b2c_env) as f:
        for line in f.readlines():
            if line.startswith("B2C_POST_EXECUTED="):
                executed = True
                console_print(line)
            elif line.startswith("B2C_PIPELINE_STATUS="):
                result_code = int(line[len("B2C_PIPELINE_STATUS=") :])
                console_print(line)

    if not executed:
        console_print(
            """[red]
************************************************************************
* I couldn't get the result of the pipeline, marking the job as failed *
************************************************************************
"""
        )
        result_code = 1

    console_print(f"exit code of the command was {result_code}")

    # ...................................................................................
    section_end("gather_results")
    #####################################################################################

    sys.exit(result_code)


if __name__ == "__main__":
    vm2c(auto_envvar_prefix="B2C")
